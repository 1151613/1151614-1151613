<%-- 
    Document   : registroExitoso
    Created on : 30/10/2019, 11:43:33 AM
    Author     : ACER
--%>
<%@page import="Dto.Cuenta"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Negocio.Banco"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/estilo.css">
        <title>Registro exitoso</title>
    </head>
    <body>
        <%
        Banco banco=(Banco)(request.getSession().getAttribute("banco"));
        request.getSession().setAttribute("banco", banco);
            
        %>
        <h1 class="register-title">Registro exitoso</h1>
        <br>
        <% for (Cuenta dato:banco.getCuentas())
        {
        %>
            <p><%=dato.toString()%></p>
        <%
            }
        %>
        <a href="./jsp/Cuenta/registrarCuenta.jsp">Ingresar otra cuenta</a>
    </body>
</html>
