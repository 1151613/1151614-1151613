<%-- 
    Document   : registroExitoso
    Created on : 30/10/2019, 04:07:31 PM
    Author     : ACER
--%>
<%@page import="Dto.Operacion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Negocio.Banco"%>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/estilo.css">
        <title>Registro exitoso</title>
    </head>
    <body>
          <%
        Banco banco=(Banco)(request.getSession().getAttribute("banco"));
        request.getSession().setAttribute("banco", banco);
            
        %>
        
        <h1 class="register-title">Registro exitoso</h1>
        <br>
        <hr>
        <% for (Operacion dato:banco.getOperaciones())
        {
        %>
            <p><%=dato.toString()%></p>
        <%
            }
        %>
        
        <hr>
        <a href="./jsp/Retirar/retirar.jsp">Retirar</a>
    </body>
</html>
