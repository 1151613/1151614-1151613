/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.time.LocalDate;

/**
 *
 * @author ACER
 */
public class Operacion implements Comparable{
    private LocalDate fechaOperacion ;
    private double saldo ;
    private long identificador;
    private Cuenta cuenta;

    public Operacion() {
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }
    
    public LocalDate getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(LocalDate fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public long getIdentificador() {
        return identificador;
    }

    public void setIdentificador(long identificador) {
        this.identificador = identificador;
    }
    

    @Override
    public int compareTo(Object o) { 
      Operacion operacion=(Operacion)o;
      return (int)(this.identificador-operacion.identificador);  
    }

    @Override
    public String toString() {
        return  "fecha de Operacion=" + fechaOperacion + ", saldo=" + saldo + ", identificador=" + identificador ;
    }

}
