/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

/**
 *
 * @author ACER
 */
public class Transferencia  extends Operacion{
    private Cuenta cuenta;
    public Transferencia() {
        super();
    }  

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }
    

    @Override
    public String toString() {
        return "Transacción:"+ super.toString()+
                "Nuevo saldo cuenta2: "+this.cuenta.getSaldo(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
