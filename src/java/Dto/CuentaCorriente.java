/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

/**
 *
 * @author estudiante
 */
public class CuentaCorriente extends Cuenta{

   public double sobregiro=1000000; 
    
    public CuentaCorriente() {
        super();
    }
    
    public String h (){
     
        return "cuenta corriente";
    }
    
   @Override
    public double getSobregiro() {
        return sobregiro;
    }

    public void setSobregiro(double sobregiro) {
        this.sobregiro = sobregiro;
    }

    @Override
    public String toString() {
        return "Cuenta Corriente " + "sobregiro= " + sobregiro + super.toString();
    }
    
    
    
}
