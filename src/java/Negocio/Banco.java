/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Dto.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.TreeSet;

/**
 *
 * @author docente
 */
public class Banco {
    TreeSet<Cliente> clientes=new TreeSet();
    TreeSet<Cuenta> cuentas=new TreeSet();
    TreeSet<Operacion> operacion = new TreeSet();
    
    public Banco() {
    }
    
    public boolean insertarCliente(long cedula, String nombre, String dir, String fecha, String email, long telefono)
    {
    
        Cliente nuevo=new Cliente();
        nuevo.setCedula(cedula);
        nuevo.setNombre(nombre);
        nuevo.setFechaNacimiento(crearFecha(fecha));
        nuevo.setDirCorrespondencia(dir);
        nuevo.setTelefono(telefono);
        nuevo.setEmail(email);
        //iría al dao , pero sólo por hoy en dinamic_memory
        return this.clientes.add(nuevo);
    }
     private Cuenta buscarCuenta(Cuenta x){
        if(this.cuentas.contains(x)){
            return this.cuentas.floor(x);
        }
        return null;
    }
    private boolean buscarCuenta2(Cuenta x){
        if(this.cuentas.contains(x)){
            return true;
        }
        return false;
    }
    
    public boolean insertarCuenta(long nroCuenta, long cedula, boolean tipo)
    {
    Cliente x=new Cliente();
    x.setCedula(cedula);
    x=buscarCliente(x);
    if(x==null)    
        return false;
    Cuenta nueva=tipo?new CuentaAhorro():new CuentaCorriente();
    nueva.setCliente(x);
    nueva.setNroCuenta(nroCuenta);
    nueva.setFechaCreacion(LocalDate.now());
    return this.cuentas.add(nueva);
    }
    public boolean insertarOperacion(long nroCuenta,long identificador,int tipo,long saldo,long nroCuenta2){
        
       
        Cuenta x= new Cuenta() ;
        x.setNroCuenta(nroCuenta);
        x=buscarCuenta(x);
        if(buscarCuenta2(x)==false){
        return false;
        }
        if(x!=null){
            if(tipo==1){
        return this.operacion.add(consignar( x,identificador,saldo));
            }else{
            if(tipo==2){ 
               
        return this.operacion.add(retirar(x,identificador,saldo)); 
                
            }else {if(tipo==3){
            Cuenta y=new Cuenta();
            y.setNroCuenta(nroCuenta2);
            y=buscarCuenta(y);
            if(y!=null){
        return this.operacion.add(transaccion(x,y,identificador,saldo));
            } } } }
        }
    return true;
    }
    private Operacion consignar(Cuenta x,long identificador,long saldo){
        Operacion nueva=new Consignacion();
        nueva.setCuenta(x);
        nueva.setIdentificador(identificador);
        nueva.setFechaOperacion(LocalDate.now());
        long nuevoSaldo=(long) (x.getSaldo()+saldo);
        x.setSaldo(nuevoSaldo);  
        nueva.setSaldo(nuevoSaldo);
        return  nueva;
    }
    private Operacion transaccion2(Cuenta x,Cuenta y,long identificador,long saldo){
        Operacion nueva=new Transferencia();
        nueva.setCuenta(x);
        nueva.setIdentificador(identificador);
        nueva.setFechaOperacion(LocalDate.now());
        long nuevoSaldo1=(long) (x.getSaldo()-saldo);
        x.setSaldo(nuevoSaldo1); 
        long nuevoSaldo2=(long) (y.getSaldo()+saldo);
        y.setSaldo(nuevoSaldo2);
        return  nueva;
    }
     private Operacion transaccion(Cuenta x,Cuenta y,long identificador,long saldo){
     
       Operacion nueva=new Transferencia();
       
       if(retirar(x,identificador,saldo)!=null && consignar(y,identificador,saldo)!=null){
            
        nueva.setCuenta(x);
        nueva.setIdentificador(identificador);
        nueva.setFechaOperacion(LocalDate.now());
        nueva.setCuenta(y);
         return nueva;
       }
        else{
            return null;
                }
     }
    
    private Operacion retirar(Cuenta x,long identificador,long saldo){
        Operacion nueva=new Retiro();
        nueva.setCuenta(x);
        nueva.setIdentificador(identificador);
        nueva.setFechaOperacion(LocalDate.now());
       
        
        
        if(x.getSaldo()+x.getSobregiro()>=saldo){
            
           
        long nuevoSaldo=(long) (x.getSaldo()-saldo);
        x.setSaldo(nuevoSaldo);
        nueva.setSaldo(nuevoSaldo);}
        else { 
            return null;
        }
        
        return  nueva; 
    }
    
    private Cliente buscarCliente(Cliente x)
    {
        if(this.clientes.contains(x))
        {
            return this.clientes.floor(x);
        }
        return null;
    }      
    
    private Cliente buscarCliente2(Cliente x)
    {
        for(Cliente y:this.clientes)
            if(y.equals(x))
                return y;
     return null;
    }
      public String informeCliente(long cedula) {
        Cliente x = new Cliente();
        String s = "";
        int i = 0;
        x.setCedula(cedula);
        if (buscarCliente(x) != null) {
            for (Object y : buscarCuentaC(x)) {
                for (Operacion o : this.operacion) {
                    i++;
                    if (o.getCuenta().equals(y)) {
                        s += "#" + i + "" + o.toString() + "</br>";
                    }
                }
            }
        }
        return s;

    }

    public String informacionCuenta(long numCuenta) {
        Cuenta x = new Cuenta();
        String s = "";
        String s2 = "";
        x.setNroCuenta(numCuenta);
        if (buscarCuenta(x) != null) {
            x = buscarCuenta(x);
            s = "Saldo: " + x.getSaldo() + "Clinte: " + x.getCliente().getNombre() + " Cedula: " + x.getCliente().getCedula() +"</br>";
            for (Object y : buscarOperacion(x)) {
                s2 += y.toString() + " </br> ";
            }
        } else {
            s = "Cuenta no encontrada";
        }
        return s + "\n" + s2;
    }

    private ArrayList buscarOperacion(Cuenta x) {
        ArrayList<Operacion> e = new ArrayList<Operacion>();
        for (Operacion o : this.operacion) {
            if (o.getCuenta().equals(x)) {
                e.add(o);
            } else {
                
            }
        }
       
        return e;
    }

    private ArrayList buscarCuentaC(Cliente x) {
        ArrayList<Cuenta> c = new ArrayList<Cuenta>();
        for (Cuenta y : this.cuentas) {
            if (y.getCliente().equals(x)) {
                c.add(y);
            } else {
                System.out.println("No se encontro nada");
            }
        }
        return c;
    }
    
    
    private LocalDate crearFecha(String fecha)
    {
        String fechas[]=fecha.split("-");
        int agno=Integer.parseInt(fechas[0]);
        int mes=Integer.parseInt(fechas[1]);
        int dia=Integer.parseInt(fechas[2]);
        return LocalDate.of(agno,mes,dia);
    }

    public TreeSet<Cliente> getClientes() {
        return clientes;
    }
    
    public TreeSet<Cuenta> getCuentas(){
        return cuentas;
    }
    public TreeSet<Operacion> getOperaciones(){
        return operacion;
    }
    
}
